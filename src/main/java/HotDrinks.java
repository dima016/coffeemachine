public enum HotDrinks {

    ESPRESSO,
    CAPPUCCINO,
    HOT_CHOCOLATE,
    TEA
}
