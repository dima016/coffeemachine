public abstract class Operation {

    public final String boilWater() {
        return "Закипятить воду";
    }

    public final String addCoffee() {
        return "Добавить кофе";
    }

    public final String addMilk() {
        return "Добавить молоко";
    }

    public final String addChocolate() {
        return "Добавить шоколад";
    }

}