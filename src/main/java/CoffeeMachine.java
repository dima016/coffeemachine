
public class CoffeeMachine extends Operation implements ICoffeeMachine {

    @Override
    public void makeDrink(HotDrinks drink) {
        switch (drink) {
            case ESPRESSO:
                makeEspresso();
                break;

            case CAPPUCCINO:
                makeCappuccino();
                break;

            case TEA:
                makeTea();
                break;

            case HOT_CHOCOLATE:
                makeHotChocolate();
                break;
        }
    }


    private void makeEspresso() {
        System.out.println(boilWater() + '\n' +
                addCoffee() + '\n' +
                "Напиток " +
                HotDrinks.ESPRESSO +
                " готов!\n");
    }

    private void makeCappuccino() {
        System.out.println(boilWater() + '\n' +
                addCoffee() + '\n' +
                addMilk() + '\n' +
                "Напиток " +
                HotDrinks.CAPPUCCINO +
                "готов!\n");
    }

    private void makeTea() {
        System.out.println(boilWater() + '\n' +
                "Вода для " +
                HotDrinks.TEA +
                " готова\n");
    }

    private void makeHotChocolate() {
        System.out.println(addChocolate() + '\n' +
                addMilk() + '\n' +
                "Напиток " +
                HotDrinks.HOT_CHOCOLATE +
                " Готов\n");
    }


}
