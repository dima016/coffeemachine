public class Main {
    public static void main(String[] args) {

        HotDrinks ESPRESSO = HotDrinks.ESPRESSO;
        HotDrinks CAPPUCCINO = HotDrinks.CAPPUCCINO;
        HotDrinks HOT_CHOCOLATE = HotDrinks.HOT_CHOCOLATE;
        HotDrinks TEA = HotDrinks.TEA;


        new CoffeeMachine().makeDrink(ESPRESSO);
        new CoffeeMachine().makeDrink(TEA);
    }
}
